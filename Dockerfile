FROM debian:jessie

RUN apt-get update -yq

RUN apt-get upgrade

# Installing a lot of packages in a single image lead to this:
# https://github.com/docker/docker/issues/5618
# So I decided to try to split downloading into three command, which would lead to smaller layers.
RUN apt-get install -yq telnet curl wget
RUN apt-get install -yq python
RUN apt-get install -yq openjdk-7-jre

RUN curl https://sdk.cloud.google.com | bash
RUN bash -c "/root/google-cloud-sdk/bin/gcloud components install beta"
RUN bash -c "/root/google-cloud-sdk/bin/gcloud components install cloud-datastore-emulator"

# Project needs to be set but won't be used
# So I used nonexistent project
RUN bash -c "/root/google-cloud-sdk/bin/gcloud config set project notimportant-allegory-br-above-bench-native"

# I **think** that this would make index.yml autogenerate.
RUN mkdir /workspace

VOLUME /workspace

WORKDIR /workspace

CMD /root/google-cloud-sdk/bin/gcloud beta emulators datastore start --host-port 0.0.0.0:5000 --data-dir /workspace
